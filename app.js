const express = require('express')
const mongoose = require('mongoose')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
// const cookieSession = require('cookie-session')
const cors = require('cors')

if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

const app = express()
const CONNECTION = process.env.MONGO_DB_URI

mongoose
  .connect(CONNECTION, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('DB connected')
  })
  .catch((err) => {
    console.log(`Error connecting to the database:\n ${err}`)
  })

const authRoutes = require('./api/routes/auth')

app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(cookieParser())
app.use(cors())
app.options('*', cors())

app.use('/auth', authRoutes)

module.exports = app

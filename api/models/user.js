const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: false,
  },
  email: {
    type: String,
    required: true,
    trim: true,
  },
  created_at: {
    type: String,
    default: new Date().toISOString(),
  },
  otp: String,
  is_google: {
    type: Boolean,
    default: false,
  },
})

module.exports = mongoose.model('User', userSchema)

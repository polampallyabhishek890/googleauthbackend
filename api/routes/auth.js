const express = require('express')

const {
  sendOtp,
  verifyOtp,
  requireSignIn,
  signOut,
  googleAuth,
} = require('../controllers/auth')

const router = express.Router()

router.post('/send-otp', sendOtp)
router.post('/verify-otp', verifyOtp)
router.post('/sign-out', signOut)
router.post('/google', googleAuth)
router.get('/verify-login', requireSignIn, (_, res) =>
  res.status(200).json({
    message: 'Success',
  }),
)

module.exports = router

module.exports = {
  OTP_LENGTH: 4,
  OTP_CONFIG: {
    upperCaseAlphabets: true,
    specialChars: false,
  },
}

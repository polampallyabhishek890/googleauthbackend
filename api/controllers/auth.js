const jwt = require('jsonwebtoken')
const { expressjwt } = require('express-jwt')
const { OAuth2Client } = require('google-auth-library')
const { sendMail } = require('../services/emailService')
const { generateOTP } = require('../services/otp')

const client = new OAuth2Client(
  process.env.CLIENT_ID,
  process.env.CLIENT_SECRET,
  'postmessage',
)

const User = require('../models/user')

module.exports.sendOtp = async (req, res) => {
  const { email } = req.body

  if (!email) {
    res.status(500).json({
      message: 'Email is required',
    })
  }

  const user = await User.findOne({
    email,
  })

  const otpGenerated = generateOTP()

  if (user) {
    user.otp = otpGenerated
    user
      .save()
      .then(() => {
        sendMail({
          to: email,
          OTP: otpGenerated,
        })
          .then(() => {
            return res.status(200).json({
              message: 'OTP Sent Successfully',
            })
          })
          .catch((err) => {
            return res.status(500).json({
              message: 'Unable to sign up, Please try again later',
            })
          })
      })
      .catch((err) => {
        return res.status(401).json({
          message: 'Unable to sign up, Please try again later',
        })
      })
  } else {
    const newUser = await User.create({
      email,
      otp: otpGenerated,
    })

    if (!newUser) {
      return res.status(500).json({
        message: 'Unable to sign up, Please try again later',
      })
    }

    sendMail({
      to: email,
      OTP: otpGenerated,
    })
      .then(() => {
        user.save().then(() => {
          return res.status(200).json({
            message: 'OTP Sent Successfully',
          })
        })
      })
      .catch((err) => {
        return res.status(500).json({
          message: 'Unable to sign up, Please try again later',
        })
      })
  }
}

module.exports.verifyOtp = async (req, res) => {
  const { email, otp } = req.body

  if (!email) {
    return res.status(500).json({
      message: 'Email is required',
    })
  }

  if (!otp) {
    return res.status(500).json({
      message: 'OTP is required',
    })
  }

  const user = await User.findOne({
    email,
  })

  if (!user) {
    return res.status(404).json({
      message: 'Invalid Email',
    })
  }

  if (user && user.otp !== otp) {
    return res.status(400).json({
      message: 'Invalid OTP',
    })
  }

  const token = jwt.sign({ _id: user.id }, process.env.JWT_SECRET)

  res.cookie('t', token, { expire: new Date() + 9999 })

  return res.status(200).json({
    token,
  })
}

module.exports.googleAuth = async (req, res) => {
  const { tokens } = await client.getToken(req.body.code)

  const ticket = await client.verifyIdToken({
    idToken: tokens.id_token,
    audience: process.env.CLIENT_ID,
  })

  const { name, email } = ticket.getPayload()
  const user = await User.findOne({
    email,
  })

  if (user) {
    const token = jwt.sign({ _id: user.id }, process.env.JWT_SECRET)

    res.cookie('t', token, { expire: new Date() + 9999 })

    return res.status(200).json({
      token,
    })
  } else {
    const newUser = await User.create({
      email,
      name,
    })

    if (!newUser) {
      return res.status(500).json({
        message: 'Unable to sign up, Please try again later',
      })
    }

    const token = jwt.sign({ _id: newUser.id }, process.env.JWT_SECRET)

    res.cookie('t', token, { expire: new Date() + 9999 })

    return res.status(200).json({
      token,
    })
  }
}

module.exports.signOut = (req, res) => {
  res.clearCookie('t')
  res.json({
    message: 'Signout success',
  })
}

module.exports.requireSignIn = expressjwt({
  secret: process.env.JWT_SECRET,
  userProperty: 'auth',
  algorithms: ['HS256'],
})
